# Python authentication web service - PAWS

## Auth sequence

Implicit Grant process

```mermaid
sequenceDiagram
    participant RO as Ressource Owner
    participant C as Client
    participant PAWS

    RO ->> C : ask access
    C -->> RO : redirect to PAWS with client identifier + acces level
    Note left of C: Auth Owner
    RO ->> PAWS : authenticate and consent
    alt Auht failed
        PAWS ->> RO : 400
    else Auth success
        PAWS -->> RO : redirect to Client redirect_URI with access token
        RO -->> C : redirect to Client redirect_URI with access token
        C ->> PAWS : request access token control
        Note left of C: verify token
        alt control failed
            PAWS -->> C : 400
            C ->> RO : 400
        else control success
            PAWS -->> C : 200
            C ->> RO : give access
        end
    end
    
```

***

## Objects

### Entities

- Credential
- Client
- Account

### Factories

- CredentialFactory
- ClientFactory
- AccountFactory

### Values objects

- Email
- Pass
- GivenToken
- GeneratedToken
- Scope
- Redirection

### Repositories

> ???

### Services

> ???

***

## Context

```mermaid
%% see syntax: https://mermaid.js.org/syntax/flowchart.html
%%{ init: { 'flowchart': { 'curve': 'stepBefore' } } }%%
flowchart

    subgraph "Support Subdomain"
        subgraph "Token_Authentication"
            style Token_Authentication rx:20,ry:20,fill:#fdc

            givenToken(GivenToken)
            requester(Requester)
        end

        subgraph "Token_delivery"
            style Token_delivery rx:20,ry:20,fill:#fdc

            generatedToken(GeneratedToken)
            redirection(Redirection)
        end
    end

    subgraph "Generic Subdomain"
        subgraph "User_management"
            style User_management rx:20,ry:20,fill:#fdc
            user_account(Userccount)
            User(User)
        end

        subgraph "Client_management"
            style Client_management rx:20,ry:20,fill:#fdc

            clientAccount(ClientAccount)
            client(Client)
        end
    end

    subgraph "Core Domain"

        subgraph "User_Authentication"
            style User_Authentication rx:20,ry:20,fill:#fdc

            Credential(Credential)
            email(Email)
            pass(Pass)
        end
    end
```

***

## Environment

```mermaid
%% see syntax: https://mermaid.js.org/syntax/flowchart.html

flowchart LR

    %% Docker env
    %% ----------
    subgraph "Docker"
    style Docker fill:#bbf,stroke:#08f,stroke-width:2px

        classDef blue stroke:#08f,stroke-width:2px
        classDef red stroke:#f00,stroke-width:2px
        
        %% Containers    
        P{Proxy}:::blue

        subgraph "Authentication Servers"
            F[Front Server]:::blue
            RS[API Server]:::blue
        end

        DB[(Database)]:::blue

    end
    
    %% Third Party
    %% -----------
    %%RO([Resource Owner]):::red
    %%C([Client]):::red
    
    %% Flow
    %% ----


```


[//]: # (***)

[//]: # ()
[//]: # (## Objects)

[//]: # ()
[//]: # (- entities)

[//]: # (  - Credential &#40;resource owner&#41;)

[//]: # (  - Client)

[//]: # (- value objects)

[//]: # (  - Token)
